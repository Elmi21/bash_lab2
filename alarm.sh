#!/bin/bash
IFSorigin=$IFS;
path='/home/elmira/alarm_clock.sh';
path1='\/home\/elmira\/alarm_clock.sh';
create_str_date_time_cron(){
IFS=$'.'$':'$' ';
local date_time=`eval "expr \"\$"$1"\""`;
date_time=( `echo "$date_time"` );
case ${date_time[2]} in
"пн" )
day_week=1;
;;
"вт" )
day_week=2;
;;
"ср" )
day_week=3;
;;
"чт" )
day_week=4;
;;
"пт" )
day_week=5;
;;
"сб" )
day_week=6;
;;
"вс" )
day_week=7;
;;
esac
IFS=$IFSorigin;
date_time_cron=${date_time[4]}' '${date_time[3]}' '${date_time[0]}' '${date_time[1]}' '$day_week' '$path;
date_time_cron11=${date_time[4]}' '${date_time[3]}' '${date_time[0]}' '${date_time[1]}' '$day_week;
eval "$2=\"$date_time_cron\"";
eval "$3=\"$date_time_cron11\"";
}
show_all_tasks(){
	echo "Все задания на данный момент:";
	crontab -l > /home/elmira/file_cron.txt;
	file="/home/elmira/file_cron.txt"
	local num_lines=0;
	while read line
	do
	mas_line=( `echo "$line"` );
	if [ "${mas_line[5]}" != "$path" ]
	then
		continue;
	fi

		case ${mas_line[4]} in
 		'1' )
 		day_week='пн';
 		;;
		'2' )
 		day_week='вт';
 		;;
 		'3' )
 		day_week='cр';
 		;;
		'4' )
 		day_week='чт';
 		;;
 		'5' )
 		day_week='пт';
 		;;
		'6' )
 		day_week='сб';
 		;;
 		'7' )
 		day_week='вс';
 		;;
	esac
	echo ${mas_line[2]}'.'${mas_line[3]}' '$day_week' '${mas_line[1]}':'${mas_line[0]};
	local_mas_str[num_lines]=`sed -n "/"${mas_line[0]}\ ${mas_line[1]}\ ${mas_line[2]}\ ${mas_line[3]}\ "/=" $file`;
	num_lines=$num_lines+1;
	done < $file
	eval "$1=( ${local_mas_str[@]} )";
}
echo "Будильник. Выберите пункт меню:";
echo "1 - Добавить задание.";
echo "2 - Редактировать задание.";
echo "3 - Удалить задание.";
echo "4 - Просмотреть все задания.";
read item;
if (($item==1))
then
	
	echo "Введите дату и время срабатывания будильника, (например 21.08 пн 09:00)";
	read date_time;
	#date_time='20.07 чт 13:00'
	create_str_date_time_cron date_time date_time_cron date_time_cron1;
	crontab -l | { cat; echo -e $date_time_cron"\n"; } | crontab
	echo "Задание добавлено!";
fi
if (($item==2))
then
	show_all_tasks mas_str;
	echo "Выберите задание, которое хотите редактировать:";
	read num_task;
	num_task=$num_task-1;
	echo "Введите новые дату и время срабатывания будильника, (например 21.08 пн 09:00)";
	read date_time1;
	create_str_date_time_cron date_time1 date_time_cron1 date_time_cron3;
	sed "${mas_str[$num_task]} s/.*/$date_time_cron3 $path1/i" $file | crontab;
	echo "Задание было изменено.";
fi
if (($item==4))
then
	show_all_tasks mas_str;
fi
if (($item==3))
then
	show_all_tasks mas_str;
	echo "Выберите задание, которое хотите удалить:";
	read num_task;
	num_task=$num_task-1;
	sed "${mas_str[$num_task]}d" $file | crontab;
	echo "Задание было удалено.";
fi
